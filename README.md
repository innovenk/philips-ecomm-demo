[![CircleCI](https://circleci.com/bb/innovenk/philips-ecomm-demo/tree/master.svg?style=svg)](https://circleci.com/bb/innovenk/philips-ecomm-demo/tree/master)

# Philips Ecomm Demo Application

## Introduction
This service is to demonstrate the capabilities of having unit testing in place for the code that is written at every stage of development.

## Service Capabilities

### Product

- GET /ecomm/product/{id}
 
- GET /ecomm/products
 
- POST /ecomm/product
 
    REQUEST
  
    ```
        {
			"category" : "Mobile"
			"description" : "pixel 2"
			"brand" : "google",
			"os" : "android lolipop"
		}
    ```
 
## Tools and Frameworks
 
 - Java 8
 - Spring Boot
 - H2 Database
 - Lombok
 - Jpa
 - Mockito