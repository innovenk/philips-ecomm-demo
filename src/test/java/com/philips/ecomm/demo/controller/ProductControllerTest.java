package com.philips.ecomm.demo.controller;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.ecomm.demo.domain.Product;
import com.philips.ecomm.demo.exception.ProductNotCreatedException;
import com.philips.ecomm.demo.exception.ProductNotFoundException;
import com.philips.ecomm.demo.service.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.junit.MockitoJUnitRunner;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ProductController controller;

    @Mock
    private ProductService service;

    private List<Product> products = new ArrayList<>();

    private Product product;

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        product  = Product.builder()
                .id(1L).category("Mobile").description("some mobile").brand("some brand").os("some os").added_by("someone").build();
        products.add(product);
    }

    @Test
    public void getProducts_shouldReturnSuccess_whenProductsAreAvailable() throws Exception {
        when(service.getProducts()).thenReturn(products);
        mockMvc.perform(MockMvcRequestBuilders.get("/ecomm/products"))
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
    }

    @Test
    public void getProducts_shouldGetAllProducts_whenProductsAreAvailable() {
        when(service.getProducts()).thenReturn(products);
        ResponseEntity<List<Product>> response = controller.getProducts();
        Assert.assertEquals(1L, response.getBody().size());
    }

    @Test
    public void getProduct_shouldReturnSuccess_whenRequestedProductIsAvailable() throws Exception {
        when(service.getProduct(1L)).thenReturn(product);
        mockMvc.perform(MockMvcRequestBuilders.get("/ecomm/product/1"))
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
    }

    @Test
    public void getProduct_shouldReturnNotFoundResponse_whenRequestedProductIsUnavailable() throws Exception {
        when(service.getProduct(1L)).thenThrow(ProductNotFoundException.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/ecomm/product/1"))
                .andExpect(MockMvcResultMatchers.status().is(404)).andReturn();
    }

    @Test
    public void getProduct_shouldGetAProduct_whenRequestedProductIsAvailable() {
        when(service.getProduct(1L)).thenReturn(product);
        ResponseEntity<Product> response = controller.getProduct(1L);
        Assert.assertEquals("some brand", response.getBody().getBrand());
    }

    @Test
    public void createProduct_shouldReturnSuccess_whenAProductIsCreatedSuccesfully() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/ecomm/product")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.convertValue(product, JsonNode.class).toString())).andReturn();

        Assert.assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void createProduct_shouldReturnBadRequest_whenAProductIsNotCreatedSuccesfully() throws Exception {
        when(service.createProduct(product)).thenThrow(ProductNotCreatedException.class);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/ecomm/product")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.convertValue(product, JsonNode.class).toString())).andReturn();

        Assert.assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    public void createProduct_shouldCreateProduct_whenAProductIsValid() {
        Product productResponse = product;
        productResponse.setId(1L);
        when(service.createProduct(product)).thenReturn(productResponse);
        ResponseEntity<Product> response = controller.createProduct(product);
        Assert.assertTrue(response.getBody().getId().equals(1L));
    }

}
