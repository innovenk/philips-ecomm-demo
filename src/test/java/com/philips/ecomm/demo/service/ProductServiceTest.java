package com.philips.ecomm.demo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.ecomm.demo.domain.Product;
import com.philips.ecomm.demo.entity.ProductEntity;
import com.philips.ecomm.demo.exception.ProductNotCreatedException;
import com.philips.ecomm.demo.exception.ProductNotFoundException;
import com.philips.ecomm.demo.repository.ProductRepository;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.junit.MockitoJUnitRunner;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private SmsMessageService smsMessageService;

    @Mock
    private ObjectMapper objectMapper;

    private Product product;
    private ProductEntity productEntity;
    private List<Product> productList =  new ArrayList<>();
    private List<ProductEntity> productEntityList =  new ArrayList<>();

    @Before
    public void setup() {
        Date date= new Date();
        long time = date. getTime();
        productEntity  = ProductEntity.builder()
                .id(1L).category("Mobile").description("some mobile").brand("google").os("some os").added_by("someone").build();
        productEntityList.add(productEntity);

        product  = Product.builder()
                .id(1L).category("Mobile").description("some mobile").brand("google").os("some os").added_by("someone").build();
        productList.add(product);
    }

    @Test
    public void getProducts_shouldGetProducts_whenProductsExist() {
        when(productRepository.findAll()).thenReturn(productEntityList);
        when(objectMapper.convertValue(productEntity, Product.class)).thenReturn(product);

        List<Product> response = productService.getProducts();
        assertEquals(1, response.size());
    }

    @Test
    public void getProduct_shouldGetProduct_whenIdIsGiven() {
        Optional<ProductEntity> optionalProductEntity = Optional.of(productEntity);
        when(productRepository.findById(1L)).thenReturn(optionalProductEntity);
        when(objectMapper.convertValue(productEntity, Product.class)).thenReturn(product);

        Product response = productService.getProduct(1L);
        assertEquals("Mobile", response.getCategory());
        assertEquals("google", response.getBrand());
        assertEquals("someone", response.getAdded_by());
        assertEquals("some mobile", response.getDescription());
        assertEquals("some os", response.getOs());
    }

    @Test(expected = ProductNotFoundException.class)
    public void getProduct_shouldThrowProductNotFoundException_whenIdIsNotFound() {
        when(productRepository.findById(1L)).thenReturn(Optional.ofNullable(null));
        productService.getProduct(1L);
    }

    @Test
    public void createProduct_shouldCreateProduct_whenCorrectProductIsGiven() {
        product.setId(1L);
        when(objectMapper.convertValue(product, ProductEntity.class)).thenReturn(productEntity);
        when(productRepository.save(productEntity)).thenReturn(productEntity);
        when(objectMapper.convertValue(productEntity, Product.class)).thenReturn(product);
        doNothing().when(smsMessageService).sendMessage();

        Product response = productService.createProduct(product);
        assertNotNull(response);

        verify(smsMessageService).sendMessage();
    }

    @Test(expected = ProductNotCreatedException.class)
    public void createProduct_shouldThrowProductNotCreatedException_whenIncorrectProductIsGiven() {
        when(objectMapper.convertValue(product, ProductEntity.class)).thenReturn(productEntity);
        when(productRepository.save(productEntity)).thenThrow(DataIntegrityViolationException.class);

        productService.createProduct(product);
    }

}