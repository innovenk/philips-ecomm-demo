DROP TABLE PRODUCT IF EXISTS;

CREATE TABLE PRODUCT
(
  id                INT  NOT NULL  IDENTITY (1, 1) PRIMARY KEY,
  category          VARCHAR(100) NOT NULL,
  description       VARCHAR(1000) NOT NULL,
  brand             VARCHAR(100) NOT NULL,
  os                VARCHAR(100) NOT NULL,
  added_by          VARCHAR(100) NOT NULL
);

INSERT INTO PRODUCT
(
    id,
    category,
    description,
    brand,
    os,
    added_by
)   VALUES  (
    1,
    'Mobile',
    'galaxy s6',
    'samsung',
    'android pie',
    'venkat'
);

INSERT INTO PRODUCT
(
    id,
    category,
    description,
    brand,
    os,
    added_by
)   VALUES  (
    2,
    'Mobile',
    'iphone 8',
    'apple',
    'ios 11',
    'jayant'
);

INSERT INTO PRODUCT
(
    id,
    category,
    description,
    brand,
    os,
    added_by
)   VALUES  (
    3,
    'Mobile',
    'pixel 3',
    'google',
    'android oreo',
    'simar'
);

INSERT INTO PRODUCT
(
    id,
    category,
    description,
    brand,
    os,
    added_by
)   VALUES  (
    4,
    'Mobile',
    'pixel 3 XL',
    'google',
    'android pie',
    'harsha'
);