package com.philips.ecomm.demo.service;

import com.philips.ecomm.demo.domain.Product;
import com.philips.ecomm.demo.entity.ProductEntity;
import com.philips.ecomm.demo.exception.ProductNotCreatedException;
import com.philips.ecomm.demo.exception.ProductNotFoundException;
import com.philips.ecomm.demo.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SmsMessageService smsMessageService;

    public Product createProduct(Product product) {
        try {
            ProductEntity productEntity = objectMapper.convertValue(product, ProductEntity.class);
            ProductEntity response = productRepository.save(productEntity);
            smsMessageService.sendMessage();
            return objectMapper.convertValue(response, Product.class);
        } catch (Exception e) {
            throw new ProductNotCreatedException();
        }
    }

    public List<Product> getProducts() {
        List<ProductEntity> productEntityList = productRepository.findAll();
        return productEntityList.stream().map(productEntity ->
                objectMapper.convertValue(productEntity, Product.class)).collect(Collectors.toList());
    }

    public Product getProduct(Long id) {
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if(productEntity.isPresent()) {
            return objectMapper.convertValue(productEntity.get(), Product.class);
        } else {
            throw new ProductNotFoundException();
        }
    }

}
