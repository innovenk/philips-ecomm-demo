package com.philips.ecomm.demo.service;

import org.springframework.stereotype.Service;

@Service
public class SmsMessageService {

    public void sendMessage() {
        System.out.println("Sending sms message to admin..");
    }

}
