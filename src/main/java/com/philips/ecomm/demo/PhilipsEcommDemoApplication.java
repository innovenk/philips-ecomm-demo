package com.philips.ecomm.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhilipsEcommDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhilipsEcommDemoApplication.class, args);
	}

}
