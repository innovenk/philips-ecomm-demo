package com.philips.ecomm.demo.controller;

import com.philips.ecomm.demo.domain.Product;
import com.philips.ecomm.demo.exception.ProductNotCreatedException;
import com.philips.ecomm.demo.exception.ProductNotFoundException;
import com.philips.ecomm.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping(value = "/ecomm/product",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createProduct(@RequestBody Product product) {
        try {
            Product response = productService.createProduct(product);
            return ResponseEntity.ok(response);
        } catch(ProductNotCreatedException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(value= "/ecomm/products",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> response = productService.getProducts();
        return ResponseEntity.ok(response);
    }

    @GetMapping(value= "/ecomm/product/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
        try {
            Product product = productService.getProduct(id);
            return ResponseEntity.ok(product);
        } catch(ProductNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
